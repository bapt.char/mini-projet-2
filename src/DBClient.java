import java.sql.*;

public class DBClient 
{
	public int id;
	public String first_name;
	public String last_name;
	public String username;
	public String email;
	public String address;
	public String tel;
	
	public DBClient() {}
	
	public DBClient(ResultSet db_row) throws SQLException
	{
		this.id = db_row.getInt("id");
		this.first_name = db_row.getString("first_name");
		this.last_name = db_row.getString("last_name");
		this.username = db_row.getString("username");
		this.email = db_row.getString("email");
		this.address = db_row.getString("address");
		this.tel = db_row.getString("tel");
		
		System.out.println("New user created: " + this.first_name + " " + this.last_name + " (tel: " + this.tel + ")");
	}
}
