import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.SystemColor;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JPasswordField;
import javax.swing.JSeparator;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

public class login 
{
	private JFrame frame;
	private JTextField field_username;
	private JLabel label_username;
	private JPasswordField field_password;
	private JLabel label_password;
	private ActionListener listener;

	/**
	 * Create the application.
	 */
	public login(ActionListener action_listeneer) 
	{
		this.listener = action_listeneer;
		initialize();
		message_box("Identifiants de test: jean.sanom:secret", "OK");
	}
	
	/**
	 * Destructeur de la classe
	 */
	public void destruct()
	{
		frame.dispose();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() 
	{
		frame = new JFrame();
		frame.getContentPane().setBackground(SystemColor.controlDkShadow);
		
		field_username = new JTextField();
		field_username.setText("jean.sannom");
		field_username.setBounds(249, 73, 199, 34);
		field_username.setToolTipText("");
		field_username.setFont(new Font("Dialog", Font.PLAIN, 15));
		field_username.setColumns(10);
		
		JLabel label_title = new JLabel("Agence Ard€nnes");
		label_title.setBounds(12, 12, 436, 44);
		label_title.setFont(new Font("DejaVu Sans", Font.PLAIN, 21));
		label_title.setForeground(SystemColor.text);
		label_title.setHorizontalAlignment(SwingConstants.CENTER);
		
		label_username = new JLabel("Nom d'utilisateur");
		label_username.setBounds(22, 73, 193, 34);
		label_username.setForeground(SystemColor.text);
		label_username.setFont(new Font("DejaVu Sans", Font.BOLD, 17));
		label_username.setLabelFor(field_username);
		
		field_password = new JPasswordField();
		field_password.setBounds(249, 124, 199, 34);
		field_password.setEchoChar('*');
		
		label_password = new JLabel("Mot de passe");
		label_password.setBounds(22, 122, 193, 34);
		label_password.setForeground(SystemColor.text);
		label_password.setFont(new Font("DejaVu Sans", Font.BOLD, 17));
		
		JButton button_connect = new JButton("Connexion");
		button_connect.addActionListener(this.listener);
		button_connect.setBounds(249, 170, 199, 34);
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(label_title);
		frame.getContentPane().add(label_username);
		frame.getContentPane().add(field_username);
		frame.getContentPane().add(label_password);
		frame.getContentPane().add(field_password);
		frame.getContentPane().add(button_connect);
		frame.setBounds(100, 100, 468, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	public String get_username()
	{
		return field_username.getText();
	}
	
	public String get_password()
	{
		return String.valueOf(field_password.getPassword());
	}
	
    public void message_box(String message, String title)
    {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE);
    }
}
