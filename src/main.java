import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;

public class main 
{
	private static login 	login_window;
	private static main_window 	main_window;
	private static SQL 		sql;
		
	public static void main(String[] args) throws Exception
	{	
		//On créer un nouveau thread pour notre fenêtre
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				//Instanciation de la classe
				login_window = new login(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) 
					{
						switch(e.getActionCommand())
						{
							case "Connexion":
							{
								on_connect();
								break;
							}
							default:
							{
								break;
							}
						}
					}
				});
			}
		});
		
		//Instanciation classe SQL
		sql = new SQL();
		
		//Initialisation du driver
		if (!sql.init_driver())
		{
			System.out.println("Impossible d'initialiser le driver");
			return;
		}
		
		//Connection à la base de données
		if (!sql.connect("localhost", "auth", "btssio", "btssio"))
		{
			System.out.println("Impossible de se connecter à la base de données");
			return;
		}
	}

	public static void on_connect()
	{
		/* On vérifie que le nom d'utilisateur n'est pas (à priori) incorrect */
		String username = login_window.get_username();
		if (username.length() < 3 || username.length() > 25)
		{
			login_window.message_box("Nom d'utilisateur invalide", "Erreur");
			return;
		}
		
		/* On vérifie que le mot de passe n'est pas (à priori) incorrect */
		String password = login_window.get_password();
		if (password.length() < 3 || password.length() > 25)
		{
			login_window.message_box("Mot de passe invalide", "Erreur");
			return;
		}
		
		/* On tente de se connecter */
		pair result = sql.login(username, password);
		if (!(boolean)result.first)
		{
			login_window.message_box("Identifiants incorrects", "Erreur");
			return;	
		}
		
		/* On est maintenant connecté */
		DBClient client = (DBClient)result.second;
		
		//On dois maintenant aller chercher les comptes du client
		String accounts = sql.get_accounts(client);
		
		//Affichage message OK
		login_window.message_box("Bienvenue, " + client.first_name + " " + client.last_name, "OK");
		
		//fermer la fenetre de login (appel du destructeur?)
		login_window.destruct();
		login_window = null;
		
		//ouvrir la fenetre des comptes
		open_main_window(accounts);
		
		//afficher les comptes
	}
	
	public static void open_main_window(String accounts_data)
	{
		//On créer un nouveau thread pour notre fenêtre
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				//Instanciation de la classe
				main_window = new main_window(accounts_data);
			}
		});
	}	

}
