import java.sql.*;

public class SQL
{
	public Connection link;
	
	public String get_accounts(DBClient client)
	{
		//Création de la requete
		String query = String.format("SELECT * FROM accounts WHERE client_id='%s';", client.id);
		
		try
		{
			//Execution de la requete
			ResultSet result = this.exec(query);
			
			//Meta donneés pour la suite
			ResultSetMetaData meta_data = result.getMetaData();
			
			//Nombre de colonnes
			int numOfCols = meta_data.getColumnCount();
			
			String resultat_final = "";
			
			//Iteration des comptes de l'utilisateur
			while (result.next())
			{
				//On ajoute à la variable resultat_final les données du compte
				resultat_final += "Comptes N°" + result.getInt("id") + "\n";
				resultat_final += "Solde: " + result.getFloat("solde") + "EUR\n";
				resultat_final += "-----------\n";
			}
			
			//On retourne le résultat
			return resultat_final;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		//Une erreur est survenue
		return "Erreur";
	}

	@SuppressWarnings("rawtypes")
	public pair login(String user, String pass)
	{
		//Création de la requete
		String query = String.format("SELECT * FROM client WHERE username='%s' AND password='%s';", user, pass);
		
		try 
		{
			//Execution de la requete
			ResultSet result = this.exec(query);
			
			//Meta donneés pour la suite
			ResultSetMetaData meta_data = result.getMetaData();
			
			//Nombre de colonnes
			int numOfCols = meta_data.getColumnCount();
			
			//Iteration des resultats (1 attendu)
			while (result.next())
			{
				//Utilisateur trouvé, on créer l'object et on retourne true
				return new pair(true, new DBClient(result));
			}
		} 
		catch (Exception e) 
		{
			
			e.printStackTrace();
		}
		
		//Aucun utilisateur trouvé
		return new pair(false, new DBClient());
	}
	
	/**
	 * @param query
	 * @return ResultSet resultat de la requete
	 * @throws Exception
	 */
	public ResultSet exec(String query) throws Exception
	{
		return link.createStatement().executeQuery(query);
	}
	
	public boolean connect(String host, String database, String user, String password)
	{
		try
		{
			link = DriverManager.getConnection("jdbc:mysql://" + host + ":3306/" + database, user, password);
			return true;
		}
		catch(SQLException e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean init_driver()
	{
		try 
		{
			Class.forName("com.mysql.cj.jdbc.Driver");
			return true;
		} 
		catch (ClassNotFoundException e) 
		{
			e.printStackTrace();
			return false;
		}
	}
	
}