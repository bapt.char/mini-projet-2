import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class main_window {

	private JFrame frame;
	private JTextArea text_area_accounts;
	
	/**
	 * Create the application.
	 */
	public main_window(String data) {
		initialize();
		this.set_accounts_data(data);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 250, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		text_area_accounts = new JTextArea();
		text_area_accounts.setLineWrap(true);
		text_area_accounts.setFont(new Font("Go", Font.PLAIN, 23));
		text_area_accounts.setText("Chargement des comptes...");
		text_area_accounts.setBounds(12, 12, 218, 256);
		frame.getContentPane().add(text_area_accounts);
		
		JButton btnNewButton = new JButton("Se Déconnecter");
		btnNewButton.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent arg0) 
			{
				System.exit(1);
			}
		});
		btnNewButton.setBounds(12, 280, 218, 25);
		frame.getContentPane().add(btnNewButton);
		frame.setVisible(true);
	}
	
	public void set_accounts_data(String info)
	{
		this.text_area_accounts.setText(info);
	}
}
